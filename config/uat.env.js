var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://uat.unlcn.com/wms-admin"',
  BASE_API_INTEGRATION: '"http://10.20.30.102:8890/lisa-integration"',
  // BASE_API_LOGIN:'"https://lisa-test.huiyunche.cn/uaa"'
  BASE_API_LOGIN: '"https://lisa-test.huiyunche.cn/uaa"'
})
