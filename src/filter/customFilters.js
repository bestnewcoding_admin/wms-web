import Vue from 'vue'

Vue.filter('parseTime', (time, cFormat) => {
  if (time === '' || time === null) {
    return ''
  }
  // 没有定义过滤格式
  if (!cFormat) {
    if ((time + '').length === 13 || (/000$/).test(time)) {
      time = parseInt(time)
      cFormat = '{y}-{m}-{d}'
    }
  }
  time = new Date(time)
  if (arguments.length === 0) {
    return null
  }

  if ((time + '').length === 10) {
    time = +time * 1000
  }

  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    date = new Date(parseInt(time))
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
})

// 付款单状态
Vue.filter('stockStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '20') {
    return '锁定'
  }
})
Vue.filter('inboundLineStatusFilter', (v) => {
  if (v === '0') {
    return '不入库'
  } else if (v === '10') {
    return '未入库'
  } else if (v === '20') {
    return '部分入库'
  } else if (v === '30') {
    return '全部入库'
  } else if (v === '40') {
    return '关闭入库'
  } else if (v === '50') {
    return '取消入库'
  } else {
    return '未设置'
  }
})
Vue.filter('inboundInspectStatusFilter', (v) => {
  if (v === '10') {
    return '全部合格'
  } else if (v === '20') {
    return '部分合格'
  } else if (v === '30') {
    return '全部破损'
  } else if (v === '0') {
    return '未质检'
  } else if (v === null) {
    return '未质检'
  } else {
    return v
  }
})
Vue.filter('keyStatusFilter', (v) => {
  if (v === '10') {
    return '未收取'
  } else if (v === '20') {
    return '已收取'
  }
  return v
})
Vue.filter('putawayGenMethodFilter', (v) => {
  if (v === '10') {
    return '手工创建'
  } else if (v === '20') {
    return '订单中心'
  } else if (v === '30') {
    return 'OTM'
  } else if (v === '40') {
    return 'APP'
  } else if (v === '50') {
    return '君马'
  } else if (v === '60') {
    return '扫码触发'
  } else if (v === '70') {
    return '道闸'
  } else if (v === '80') {
    return '自动触发'
  } else {
    return v
  }
})
// 头出库状态
Vue.filter('noticeHeadStatusFilter', (v) => {
  if (v === '10') {
    return '未出库'
  } else if (v === '20') {
    return '部分出库'
  } else if (v === '30') {
    return '全部出库'
  } else if (v === '40') {
    return '关闭出库'
  } else if (v === '50') {
    return '取消出库'
  } else {
    return v
  }
})
// 明细出库状态
Vue.filter('noticeLineStatusFilter', (v) => {
  if (v === '0') {
    return '不出库'
  } else if (v === '10') {
    return '未出库'
  } else if (v === '20') {
    return '部分出库'
  } else if (v === '30') {
    return '全部出库'
  } else if (v === '40') {
    return '关闭出库'
  } else if (v === '50') {
    return '取消出库'
  } else {
    return v
  }
})
// 头备料状态
Vue.filter('prepareHeadStatusFilter', (v) => {
  if (v === '10') {
    return '未备料'
  } else if (v === '20') {
    return '开始执行'
  } else if (v === '30') {
    return '部分备料'
  } else if (v === '40') {
    return '全部完成'
  } else {
    return v
  }
})
// 明细备料状态
Vue.filter('prepareLineStatusFilter', (v) => {
  if (v === '10') {
    return '未开始'
  } else if (v === '20') {
    return '已开始'
  } else if (v === '30') {
    return '已完成'
  } else if (v === '40') {
    return '已取消'
  } else {
    return v
  }
})
// 异常分类
Vue.filter('exceptionTaskNodeFilter', (v) => {
  if (v === '0') {
    return '指令'
  } else if (v === '10') {
    return '寻车'
  } else if (v === '20') {
    return '移车'
  } else if (v === '30') {
    return '提车'
  } else if (v === '40') {
    return '收车质检'
  } else if (v === '41') {
    return '入库移车'
  } else if (v === '42') {
    return '分配入库'
  } else if (v === '50') {
    return '出库备车'
  } else if (v === '51') {
    return '出库确认'
  } else if (v === '60') {
    return '装车交验'
  } else {
    return v
  }
})
// 异常状态
Vue.filter('exceptionStatusFilter', (v) => {
  if (v === '10') {
    return '登记'
  } else if (v === '20') {
    return '处理'
  } else if (v === '30') {
    return '关闭'
  } else {
    return v
  }
})
// 异常处理状态
Vue.filter('exceptionDealStatusFilter', (v) => {
  if (v === '10') {
    return '未处理'
  } else if (v === '20') {
    return '处理中'
  } else if (v === '30') {
    return '关闭'
  } else if (v === '40') {
    return '让步'
  } else if (v === '50') {
    return '已处理'
  } else {
    return v
  }
})
// 异常处理方式
Vue.filter('exceptionDealTypeFilter', (v) => {
  if (v == null) {
    return '未处理'
  }
  const p_v = parseInt(v)
  if (p_v === 10) {
    return '返厂维修'
  } else if (p_v === 20) {
    return '出库维修'
  } else if (p_v === 30) {
    return '库内维修'
  } else if (p_v === 40) {
    return '带伤发运'
  } else if (p_v === 50) {
    return '异常关闭'
  } else if (p_v === 60) {
    return '驳回发运'
  } else {
    return p_v
  }
})
// 任务类型
Vue.filter('taskTypeFilter', (v) => {
  if (v === '10') {
    return '寻车'
  } else if (v === '20') {
    return '移车'
  } else if (v === '30') {
    return '提车'
  } else {
    return v
  }
})
// 任务状态
Vue.filter('taskStatusFilter', (v) => {
  if (v === '10') {
    return '创建'
  } else if (v === '20') {
    return '开始'
  } else if (v === '30') {
    return '完成'
  } else if (v === '50') {
    return '取消'
  } else {
    return v
  }
})
// 运单状态
Vue.filter('orderReleaseStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '50') {
    return '取消'
  } else {
    return v
  }
})
// 出入库类型
Vue.filter('boundTypeFilter', (v) => {
  if (v === '10') {
    return '入库'
  } else if (v === '20') {
    return '出库'
  } else if (v === '30') {
    return '移库'
  } else if (v === '40') {
    return '其他'
  } else {
    return v
  }
})
// 库区类型
Vue.filter('storeAreaTypeFilter', (v) => {
  if (v === '10') {
    return '普通库区'
  } else if (v === '20') {
    return '临时库区'
  } else if (v === '30') {
    return '虚拟库区'
  } else {
    return v
  }
})
// 库区状态
Vue.filter('storeAreaStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '20') {
    return '禁用'
  } else {
    return v
  }
})
// 库位类型
Vue.filter('storeLocationTypeFilter', (v) => {
  if (v === '10') {
    return '普通库区'
  } else if (v === '20') {
    return '临时库区'
  } else if (v === '30') {
    return '虚拟库区'
  } else if (v === '40') {
    return '大库区'
  } else {
    return v
  }
})
// 库位状态
Vue.filter('storeLocationStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '20') {
    return '失效'
  } else {
    return v
  }
})
// 仓库状态
Vue.filter('storeHouseFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '20') {
    return '失效'
  } else {
    return v
  }
})
// 订单状态
Vue.filter('releaseStatusFilter', (v) => {
  switch (v) {
    case '10':
      return '正常'
    case '50':
      return '取消'
    case 'OR_CREATED':
      return '已下发'
    case 'BS_CREATED':
      return '已调度'
    case 'OR_FIND':
      return '已寻车'
    case 'WMS_MOVE':
      return '已移车'
    case 'WMS_PICKUP':
      return '提车开始'
    case 'BS_INBOUND':
      return '已入库'
    case 'WMS_OUTBOUND':
      return '已出库'
    case 'WMS_HANDOVER':
      return '已交验'
    case 'BS_DISPATCH':
      return '已发运'
    case 'BS_ARRIVAL':
      return '已运抵'
    case 'BS_POD':
      return '已回单'
    case 'OR_CLOSED':
      return '已结算'
    default:
      return v
  }
})
// 发运状态
Vue.filter('shipStatusFilter', (v) => {
  if (v === 'BS_DISPATCH') {
    return '已发运'
  } else {
    return '未发运'
  }
})

const that = this
// 通过 过滤方法名String 找到和使用对应过滤方法Func
Vue.filter('geval', (str, func1) => {
  if (func1 === 'parseTime("{y}-{m}-{d} {h}:{i}:{s}")') {
    return that.filter('parseTime', str, '{y}-{m}-{d} {h}:{i}:{s}')
  } else if (func1 === 'parseTime("{y}-{m}-{d}")') {
    return that.filter('parseTime', str, '{y}-{m}-{d}')
  } else {
    return that.filter(func1, str)
  }
})

