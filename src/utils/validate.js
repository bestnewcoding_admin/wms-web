/**
 * Created by jiachenpan on 16/11/18.
 */

export function isvalidUserKey(str) {
  return str.trim().length > 0
}

/* 合法uri*/
export function validateURL(textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母*/
export function validatAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}
// 验证手机号
export function phone(rule, value, callback) {
  function isPoneAvailable(str) {
    const myreg = /^[1][3,4,5,7,8][0-9]{9}$/
    const myreg1 = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/
    const myreg2 = /^\d{7,8}$/
    if (!myreg.test(str) && !myreg1.test(str)) {
      if (myreg2.test(str)) {
        return 'ADD'
      } else {
        return false
      }
    } else {
      return true
    }
  }
  if (!(value)) {
    callback(new Error('电话不能为空'))
  } else if (isPoneAvailable(value) === 'ADD') {
    callback(new Error('请添加区号; 形如:XXX-XXXXXXX'))
  } else if (isPoneAvailable(value)) {
    callback()
  } else {
    callback(new Error('非法电话'))
  }
}

// 验证手机号
export function newPhone(rule, value, callback) {
  function isPoneAvailable(str) {
    const myreg = /^[1][3,4,5,7,8][0-9]{9}$/
    const myreg1 = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/
    const myreg2 = /^\d{7,8}$/
    if (!myreg.test(str) && !myreg1.test(str)) {
      if (myreg2.test(str)) {
        return 'ADD'
      } else {
        return false
      }
    } else {
      return true
    }
  }
  if (!(value)) {
    callback()
  } else if (isPoneAvailable(value) === 'ADD') {
    callback(new Error('请添加区号; 形如:XXX-XXXXXXX'))
  } else if (isPoneAvailable(value)) {
    callback()
  } else {
    callback(new Error('非法电话'))
  }
}
// 验证正整数 范围(0-9999)
export function regU(rule, value, callback) {
  const reg = /^[1-9]\d{0,3}$/
  if (reg.test(value) || value === null) {
    callback()
  } else {
    callback(new Error('非法数字或超过限制(0~9999)'))
  }
}
