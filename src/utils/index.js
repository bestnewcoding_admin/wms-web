/**
 * Created by jiachenpan on 16/11/18.
 */

export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse('{"' + decodeURIComponent(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}')
}

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (('' + time).length === 10) time = parseInt(time) * 1000
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

export function formatTime(time, option) {
  time = +time * 1000
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) { // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return d.getMonth() + 1 + '月' + d.getDate() + '日' + d.getHours() + '时' + d.getMinutes() + '分'
  }
}

/**
 * 前提数组内的item 必须有唯一标识参数idKey,例：id
 * 引用 import * as Utils from '@/utils/index'
 * 使用 const new_arr = Utils.addArrayFunc(arr1, arr2, idKey) // 合并数组
 * @param arr1: [{ id:1, num:11 }, { id:2, num:12}, { id:3, num:13}]
 * @param arr2: [{ id:2, num:11 }, { id:3, num:13}, { id:4, num:14}]
 * @param idKey 'id'
 * @return res_arr: [ { id:1, num:11 }, { id:2, num:11 }, { id:3, num:13}, { id:4, num:14}]
 */
export function addArrayFunc(arr1, arr2, idKey) {
  const init_arr = arr1.concat(arr2)
  const res_arr = []
  for (let i = 0; i < init_arr.length; i++) {
    // 如果当前数组的第i已经保存进了临时数组，那么跳过，
    // 否则把当前项push到临时数组里面
    if (res_arr.length === 0) {
      res_arr.push(init_arr[i])
    } else {
      let repeat = false
      for (let c = 0; c < res_arr.length; c++) {
        if (res_arr[c][idKey] === init_arr[i][idKey]) {
          repeat = true
        }
      }
      if (!repeat) {
        res_arr.push(init_arr[i])
      }
    }
  }
  return res_arr
}

/**
 * 前提数组内的item 必须有唯一标识参数idKey,例：id
 * 引用 import * as Utils from '@/utils/index'
 * 使用 const new_arr = Utils.array_cut_item(arr, item, idKey) // 减去数组内某对象
 * @param arr [{ id:1, num:11 }, { id:2, num:12}, { id:3, num:13}]
 * @param item { id:1, num:11 }
 * @param idKey 'id'
 * @return arr [{ id:2, num:12}, { id:3, num:13}]
 */
export function array_cut_item(arr, item, idKey) {
  for (let j = 0; j < arr.length; j++) {
    if (arr[j][idKey] === item[idKey]) {
      arr.splice(j, 1)
    }
  }
  return arr
}

