/**
 * 引用: import Print from '@/utils/print'
 *  import Vue from 'vue'
 *  Vue.use(Print)
 * demo:
 * <div ref="print1">
 *   <div>需要打印的demo</div>
 *   <div class="no-print"> 不需要打印的內容</div>
 * </div>
 * printClick() {
 *   this.$print(this.$refs.print1)
 * }
 */

// 打印类属性、方法定义
/* eslint-disable */
const Print =function(dom, options) {
  if (!(this instanceof Print)) return new Print(dom, options);

  this.options = this.extend({
    'noPrint': '.no-print'
  }, options);

  if ((typeof dom) === "string") {
    this.dom = document.querySelector(dom);
  } else {
    this.dom = dom;
  }

  this.init();
};
Print.prototype = {
  init: function () {
    var content = this.getStyle() + this.getHtml();
    this.writeIframe(content);
  },
  getExplorer() {
    var explorer = window.navigator.userAgent ;
    //ie
    if (explorer.indexOf("MSIE") >= 0) {
      return "IE";
    }
    //firefox
    else if (explorer.indexOf("Firefox") >= 0) {
      return "Firefox";
    }
    //Chrome
    else if(explorer.indexOf("Chrome") >= 0){
      return "Chrome";
    }
    //Opera
    else if(explorer.indexOf("Opera") >= 0){
      return "Opera";
    }
    //Safari
    else if(explorer.indexOf("Safari") >= 0){
      return "Safari";
    }
  },
  pageSetupNull(){
    var h_key_root,h_key_path,h_key_key;
    h_key_root="HKEY_CURRENT_USER";
    h_key_path="\\Software\\Microsoft\\Internet Explorer\\PageSetup\\";
    try{
      var RegWsh = new ActiveXObject("WScript.Shell");
      h_key_key="header";
      RegWsh.RegWrite(h_key_root+h_key_path+h_key_key,"");
      h_key_key="footer";
      RegWsh.RegWrite(h_key_root+h_key_path+h_key_key,"");
    }catch(e){}
  },
  extend: function (obj, obj2) {
    for (var k in obj2) {
      obj[k] = obj2[k];
    }
    return obj;
  },

  getStyle: function () {
    var str = "",
      styles = document.querySelectorAll('style,link');
    for (var i = 0; i < styles.length; i++) {
      str += styles[i].outerHTML;
    }
    str += "<style>" + (this.options.noPrint ? this.options.noPrint : '.no-print') + "{display:none;}</style>";

    return str;
  },

  getHtml: function () {
    var inputs = document.querySelectorAll('input');
    var textareas = document.querySelectorAll('textarea');
    var selects = document.querySelectorAll('select');

    for (var k in inputs) {
      if (inputs[k].type == "checkbox" || inputs[k].type == "radio") {
        if (inputs[k].checked == true) {
          inputs[k].setAttribute('checked', "checked")
        } else {
          inputs[k].removeAttribute('checked')
        }
      } else if (inputs[k].type == "text") {
        inputs[k].setAttribute('value', inputs[k].value)
      }
    }

    for (var k2 in textareas) {
      if (textareas[k2].type == 'textarea') {
        textareas[k2].innerHTML = textareas[k2].value
      }
    }

    for (var k3 in selects) {
      if (selects[k3].type == 'select-one') {
        var child = selects[k3].children;
        for (var i in child) {
          if (child[i].tagName == 'OPTION') {
            if (child[i].selected == true) {
              child[i].setAttribute('selected', "selected")
            } else {
              child[i].removeAttribute('selected')
            }
          }
        }
      }
    }

    return this.dom.outerHTML;
  },

  writeIframe: function (content) {
    var w, doc, iframe = document.createElement('iframe'),
      f = document.body.appendChild(iframe);
    iframe.id = "myIframe";
    iframe.style = "position:absolute;width:0;height:0;top:-10px;left:-10px;";

    w = f.contentWindow || f.contentDocument;
    doc = f.contentDocument || f.contentWindow.document;
    doc.open();
    doc.write(content);
    doc.close();
    // 去掉页眉 页脚
    if (this.getExplorer() === 'IE') {
      this.pageSetupNull();
    }
    this.toPrint(w);

    setTimeout(function () {
      document.body.removeChild(iframe)
    }, 600)
  },

  toPrint: function (frameWindow) {
    try {
      setTimeout(function () {
        frameWindow.focus();
        try {
          if (!frameWindow.document.execCommand('print', false, null)) {
            frameWindow.print();
          }
        } catch (e) {
          frameWindow.print();
        }
        frameWindow.close();
      }, 500);
    } catch (err) {
      console.log('err', err);
    }
  }
};

const MyPlugin = {}
MyPlugin.install = function (Vue, options) {
  // 4. 添加实例方法
  Vue.prototype.$print = Print
}
export default MyPlugin
