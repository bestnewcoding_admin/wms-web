import fetch from '@/utils/fetch'

// 加载树形数据
export function queryPage(data) {
  return fetch({
    url: '/batchJourna/queryPage',
    method: 'post',
    data
  })
}

// 批量生成分录
export function batchToCreateJournalizing(data) {
  return fetch({
    url: '/batchJourna/batchToCreateJournalizing',
    method: 'post',
    data
  })
}

// 重新生成分录
export function batchToReCreateJournalizing(data) {
  return fetch({
    url: '/batchJourna/batchToReCreateJournalizing',
    method: 'post',
    data
  })
}

// 重新生成分录
export function uploadNcVoucher(data) {
  return fetch({
    url: '/batchJourna/uploadNcVoucher',
    method: 'post',
    data
  })
}
