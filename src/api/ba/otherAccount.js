import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/otherAccount/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/otherAccount/create',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/otherAccount/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/otherAccount/delete',
    method: 'post',
    data
  })
}

export function queryCompanyList() {
  return fetch({
    url: '/company/getAll',
    method: 'get'
  })
}

export function handleFreeze(data) {
  return fetch({
    url: '/otherAccount/handleFreeze',
    method: 'post',
    data
  })
}

export function handleCancellation(data) {
  return fetch({
    url: '/otherAccount/handleCancellation',
    method: 'post',
    data
  })
}

export function handleUnFreeze(data) {
  return fetch({
    url: '/otherAccount/handleUnFreeze',
    method: 'post',
    data
  })
}

export function queryCurrencyList(data) {
  return fetch({
    url: '/currencyType/getAllList',
    method: 'get'
  })
}

