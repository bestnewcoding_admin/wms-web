import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/measureUnit/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function add(data) {
  return fetch({
    url: '/measureUnit/add',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return fetch({
    url: '/measureUnit/deleteMeasureUnit',
    method: 'post',
    data
  })
}

// 根据id查找计量单位信息
export function findById(id) {
  return fetch({
    url: '/measureUnit/findById?id=' + id,
    method: 'get'
  })
}

// 更新
export function modify(data) {
  return fetch({
    url: '/measureUnit/modify',
    method: 'post',
    data
  })
}

//
export function getMeasureUnitList() {
  return fetch({
    url: '/measureUnit/getMeasureUnitList',
    method: 'get'
  })
}
