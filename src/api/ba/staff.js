import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listStaff(data) {
  return fetch({
    url: '/staff/list',
    method: 'post',
    data
  })
}

/**
 * 新增人员
 * @param data
 */
export function addStaff(data) {
  return fetch({
    url: '/staff/add',
    method: 'post',
    data
  })
}

/**
 * 获取人员
 * @param data
 */
export function getStaff(id) {
  return fetch({
    url: '/staff/get/' + id,
    method: 'get'
  })
}

/**
 * 更新人员
 * @param data
 */
export function updateStaff(data) {
  return fetch({
    url: '/staff/update',
    method: 'put',
    data
  })
}

/**
 * 删除人员
 * @param data
 */
export function deleteStaff(data) {
  return fetch({
    url: '/staff/delete',
    method: 'delete',
    data
  })
}

/**
 * 获取所有人员
 * @param data
 */
export function getAllStaff() {
  return fetch({
    url: '/staff/getAll',
    method: 'get'
  })
}
