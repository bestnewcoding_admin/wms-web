import fetch from '@/utils/fetch'
// 查询库存
export function queryStock(data) {
  return fetch({
    url: '/stock/queryPage',
    method: 'post',
    data: data
  })
}
// 查询可用库位
export function queryAllArea(data) {
  return fetch({
    url: '/warehouse/queryAllArea',
    method: 'post',
    data: data
  })
}
// 调整库存库位
export function updateStockLocation(data) {
  return fetch({
    url: '/stock/updateStockLocation',
    method: 'post',
    data: data
  })
}
// 锁定库存
export function lockStockForBatch(data) {
  return fetch({
    url: '/stock/lockStock',
    method: 'post',
    data: data
  })
}
// 解锁库存
export function unlockStockForBatch(data) {
  return fetch({
    url: '/stock/unlockStock',
    method: 'post',
    data: data
  })
}
// 库存导出所有
export function exportStockData(data) {
  return fetch({
    url: '/stock/exportStockData',
    method: 'post',
    data: data
  })
}
