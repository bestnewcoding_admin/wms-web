import fetch from '@/utils/fetch'
export function listInpsect(data) {
  return fetch({
    url: '/inboundInspect/listInspect',
    method: 'post',
    data: data
  })
}
// 导出
export function exportExcpList(data) {
  return fetch({
    url: '/inboundInspect/exportInspect',
    method: 'post',
    data: data
  })
}

