import fetch from '@/utils/fetch'
export function selectPutawayPage(data) {
  return fetch({
    url: '/inboundPutaway/selectPutawayPage',
    method: 'post',
    data: data
  })
}
// 收取钥匙-获取入库信息
export function getReceiveKeyInfo(data) {
  return fetch({
    url: '/inboundPutaway/getReceiveKeyInfo',
    method: 'post',
    data: data
  })
}
// 收取钥匙-确认收取钥匙
export function confirmReceiveKey(data) {
  return fetch({
    url: '/inboundPutaway/confirmReceiveKey',
    method: 'post',
    data: data
  })
}
// 收取钥匙时打印二维码
export function getKeyPrintInfo(data) {
  return fetch({
    url: '/inboundPutaway/getKeyPrintInfo',
    method: 'post',
    data: data
  })
}
// 入库记录导出
export function exportData(data) {
  return fetch({
    url: '/inboundPutaway/exportINRData',
    method: 'post',
    data: data
  })
}

