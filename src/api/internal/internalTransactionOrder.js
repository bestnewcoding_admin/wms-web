import fetch from '@/utils/fetch'

// 分页
export function queryPage(data) {
  return fetch({
    url: '/transactionOrdre/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addTransactionOrder(data) {
  return fetch({
    url: '/transactionOrdre/addTransactionOrder',
    method: 'post',
    data
  })
}

// 编辑
export function updateTransactionOrder(data) {
  return fetch({
    url: '/transactionOrdre/updateTransactionOrder',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/transactionOrdre/deleteForBatch',
    method: 'post',
    data
  })
}
