import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/initialCost/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCaInitialCost(data) {
  return fetch({
    url: '/initialCost/addCaInitialCost',
    method: 'post',
    data
  })
}

// 编辑
export function updateCaInitialCost(data) {
  return fetch({
    url: '/initialCost/updateCaInitialCost',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/initialCost/deleteForBatch',
    method: 'post',
    data
  })
}

// 根据id获取
export function getCaInitialCostById(id) {
  return fetch({
    url: '/initialCost/getCaInitialCostById/' + id,
    method: 'get'
  })
}

// 根据id获取
export function getDetailList(headId) {
  return fetch({
    url: '/initialCost/getDetailList/' + headId,
    method: 'get'
  })
}
