import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listCaExpenses(data) {
  return fetch({
    url: '/caExpenses/list',
    method: 'post',
    data
  })
}

/**
 * 新增应付单类型
 * @param data
 */
export function addCaExpenses(data) {
  return fetch({
    url: '/caExpenses/add',
    method: 'post',
    data
  })
}

/**
 * 获取应收单类型
 * @param data
 */
export function getCaExpenses(id) {
  return fetch({
    url: '/caExpenses/get/' + id,
    method: 'get'
  })
}

/**
 * 更新应付单类型
 * @param data
 */
export function updateCaExpenses(data) {
  return fetch({
    url: '/caExpenses/update',
    method: 'put',
    data
  })
}

/**
 * 删除应付单类型
 * @param data
 */
export function deleteCaExpenses(data) {
  return fetch({
    url: '/caExpenses/delete',
    method: 'delete',
    data
  })
}
