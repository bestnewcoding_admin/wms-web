import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/accountingSubjct/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function create(data) {
  return fetch({
    url: '/accountingSubjct/create',
    method: 'post',
    data
  })
}

// 编辑
export function update(data) {
  return fetch({
    url: '/accountingSubjct/update',
    method: 'post',
    data
  })
}

// 删除
export function deleteByIdList(data) {
  return fetch({
    url: '/accountingSubjct/deleteByIdList',
    method: 'post',
    data
  })
}

// 根据ID获取
export function getAccountingSubjectById(id) {
  return fetch({
    url: '/accountingSubjct/getAccountingSubjectById/' + id,
    method: 'get'
  })
}
