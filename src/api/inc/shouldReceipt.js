import fetch from '@/utils/fetch'
export function queryShouldReceiptPage(data) {
  return fetch({
    url: '/shouldReceipt/queryShouldReceiptPage',
    method: 'post',
    data
  })
}

// 新增
export function addShouldReceipt(data) {
  return fetch({
    url: '/shouldReceipt/addShouldReceipt',
    method: 'post',
    data
  })
}

// 编辑
export function updateShouldReceipt(data) {
  return fetch({
    url: '/shouldReceipt/updateShouldReceipt',
    method: 'post',
    data
  })
}

// 删除
export function deletedForBatch(data) {
  return fetch({
    url: '/shouldReceipt/deletedForBatch',
    method: 'post',
    data
  })
}

// 审核
export function confirmShouldReceipt(data) {
  return fetch({
    url: '/shouldReceipt/confirmShouldReceipt',
    method: 'post',
    data
  })
}

// 获取详情
export function getShouldReceiptById(id) {
  return fetch({
    url: '/shouldReceipt/getShouldReceiptById/' + id,
    method: 'get'
  })
}
