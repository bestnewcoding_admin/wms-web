import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/incomeOrderLog/queryPage',
    method: 'post',
    data
  })
}
