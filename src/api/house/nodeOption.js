import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/deliveryPoint/listPoint',
    method: 'post',
    data
  })
}
// 从integration 获取节点基础信息
export function getStoreCode(data) {
  return fetch({
    baseURL: process.env.BASE_API_INTEGRATION,
    url: '/origination/queryPageOrigination ',
    method: 'post',
    data
  })
}
// 获取所有节点信息
export function getNodeOption() {
  return fetch({
    url: '/deliveryPoint/getNodeOption ',
    method: 'get'
  })
}
export function create(data) {
  return fetch({
    url: '/deliveryPoint/savePoint',
    method: 'post',
    data
  })
}
export function update(data) {
  return fetch({
    url: '/deliveryPoint/updatePoint',
    method: 'post',
    data
  })
}
