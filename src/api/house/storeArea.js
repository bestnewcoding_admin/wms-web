import fetch from '@/utils/fetch'

// 库区查询
export function queryByPage(data) {
  return fetch({
    url: '/storeArea/queryByPage',
    method: 'post',
    data: data
  })
}

// 新增库区
export function addStoreArea(data) {
  return fetch({
    url: '/storeArea/addStoreArea',
    method: 'post',
    data: data
  })
}

// 编辑库区
export function updateStoreArea(data) {
  return fetch({
    url: '/storeArea/updateStoreArea',
    method: 'post',
    data: data
  })
}
