import fetch from '@/utils/fetch'

// 库位查询
export function queryLocationByPage(data) {
  return fetch({
    url: '/storeLocation/queryLocationByPage',
    method: 'post',
    data: data
  })
}

// 新增库位
export function addStoreLocation(data) {
  return fetch({
    url: '/storeLocation/addStoreLocation',
    method: 'post',
    data: data
  })
}

// 编辑库位
export function updateStoreLocation(data) {
  return fetch({
    url: '/storeLocation/updateStoreLocation',
    method: 'post',
    data: data
  })
}

// 导入库位
export function importLocationInit(data) {
  return fetch({
    url: '/storeLocation/importLocationInit',
    method: 'post',
    data: data
  })
}
