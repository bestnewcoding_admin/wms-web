import fetch from '@/utils/fetch'

// 列表查询
export function getOutboundNoticeList(data) {
  return fetch({
    url: '/outboundNotice/getOutboundNoticeList',
    method: 'post',
    data: data
  })
}
// 列表查询
export function exportData(data) {
  return fetch({
    url: '/outboundNotice/exportONData',
    method: 'post',
    data: data
  })
}
