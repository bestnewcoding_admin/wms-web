import fetch from '@/utils/fetch'

// 列表查询
export function queryKeyPreparePage(data) {
  return fetch({
    url: '/keyPrepare/queryKeyPreparePage',
    method: 'post',
    data: data
  })
}
export function getKeyPrepareDetail(data) {
  return fetch({
    url: '/keyPrepare/getKeyPrepareDetail',
    method: 'post',
    data: data
  })
}
export function updateKeyPrepareStatus(data) {
  return fetch({
    url: '/keyPrepare/updateKeyPrepareStatus',
    method: 'post',
    data: data
  })
}
